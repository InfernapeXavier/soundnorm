# soundnorm
 
> Run Script: "dev"

### Design
- My design is pretty simple. I am using Express for the server and Redis as a caching layer.
- A Redis server needs to be run locally (I am using the default port) for the application to work.
- I looked through the internet about how Alexa sounds need to be normalized but could not find more than the following:
    1. https://developer.amazon.com/en-US/docs/alexa/flashbriefing/normalizing-the-loudness-of-audio-content.html
    2. https://developer.amazon.com/en-US/docs/alexa/alexa-voice-service/recommended-media-support.html
- The API has a bare-bones front-end for input. Each input has a default value as well, except for 