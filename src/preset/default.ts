exports.load = function(ffmpeg) {
    ffmpeg
    .audioFilters([
        {
          filter: 'loudnorm',
          options: 'I=-14:TP=-3:LRA=11:print_format=json'
        }
}