import path from "path";
import fs from "fs";
import { Request, Response } from "express";
import { nanoid } from "nanoid";
import { cacheAdd, cacheCheck, redisConnect } from "./utils/caching";
import { processFile } from "./utils/normalizing";
import { fileDownloader } from "./utils/downloading";
import { fileUploader } from "./utils/uploading";
import { configParams, serverResponse } from "./types";
import { validateInput } from "./utils";

export const rootHandler = (_req: Request, res: Response) => {
  res.sendFile(path.join(__dirname + "/views/index.html"));
};

export const submitHandler = async (req: Request, res: Response) => {
  const urlInput: string = req.body.urlInput;
  const intLoudness: string = req.body.intLoudness;
  const lra: string = req.body.lra;
  const requiredBitrate: string = req.body.bitrate;

  let finalResponse: serverResponse = {
    inCache: false,
    downloadUrl: "NONE",
  };

  let configParams: configParams = {
    urlInput: urlInput,
    intLoudness: intLoudness || "14",
    lra: lra || "11",
    bitrate: requiredBitrate,
  };

  // Handles incorrect url
  if (validateInput(configParams)) {
    console.log("Valid Inputs");
  } else {
    console.log("Invalid Inputs");
    return res
      .status(400)
      .send("BAD inputs, please check your inputs and retry.");
  }

  // TODO Handle Redis Errors
  const client = redisConnect();
  // console.log(client);
  // if (!client.connected) {
  //   return res
  //     .status(400)
  //     .send("Redis refused connection, please check your redis server.");
  // }

  let cacheCheckResponse;
  try {
    cacheCheckResponse = await cacheCheck(configParams, client);
  } catch (err) {
    return res
      .status(400)
      .send("Cannot connect to Redis, check your server. Redis said: " + err.code);
  }
  // console.log(
  //   "Cache Checked, response: " + JSON.stringify(cacheCheckResponse, null, 2)
  // );

  if (cacheCheckResponse.inCache) {
    finalResponse.inCache = true;
    finalResponse.downloadUrl = cacheCheckResponse.downloadUrl;
  } else {
    const fileName: string = nanoid();
    const downloadSuccess: boolean = await fileDownloader(
      fileName,
      configParams
    );
    // console.log("DownloadStatus: " + downloadSuccess);
    console.log("FileName: " + fileName);

    if (downloadSuccess) {
      console.log("\nPROCESSING FILE\n");
      const processResponse = await processFile(fileName, configParams);

      if (processResponse.status) {
        try {
          // Delete downloaded file
          fs.unlink("./download/" + fileName, (err) => {
            if (err) {
              console.error("Error: " + err);
              return err;
            } else {
              console.log("Successfully deleted downloaded file");
            }
          });

          let uploadResponse = await fileUploader(processResponse.fullFileName);
          // console.log(uploadResponse.hostedUrl);
          console.log("Processed file uploaded");

          // Delete file after upload
          fs.unlink("./processed/" + processResponse.fullFileName, (err) => {
            if (err) {
              console.error("Error: " + err);
              return err;
            } else {
              console.log("Successfully deleted processed file");
            }
          });

          finalResponse.downloadUrl = uploadResponse.hostedUrl;
          cacheAdd(configParams, uploadResponse.hostedUrl, client);
        } catch (err) {
          console.error(err);
        }
      }
    }
  }

  console.log("Final Response: " + JSON.stringify(finalResponse, null, 2));

  return res.json(finalResponse.downloadUrl);
};
