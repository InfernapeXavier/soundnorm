import { configParams } from "./types";

const validURL = (url: string) => {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(url);
} // https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url

export const validateInput = (params: configParams): boolean => {
  const checkUrl = validURL(params.urlInput);
  const checkLRA = !isNaN(+params.lra);
  const checkIntLoudness = !isNaN(+params.intLoudness);
  const checkBitrate = !isNaN(+params.bitrate);

  if (checkUrl && checkLRA && checkIntLoudness && checkBitrate) {
    return true;
  } else {
    return false;
  }
}