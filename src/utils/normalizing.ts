import ffmpeg from "fluent-ffmpeg";
import { configParams, formatResponse, normalizerResponse } from "../types";

export const processFile = async (
  fileName: string,
  configParams: configParams
): Promise<normalizerResponse> => {
  const bitrate = await bitrateCheck(fileName).catch((err) => {
    console.error(err);
  });
  //   console.log("Bitrate Check: " + bitrateResponse);

  const formatResponse: formatResponse = await formatGetter(fileName);

  const intLoudness: string = configParams["intLoudness"];
  const lra: string = configParams["lra"];

  return new Promise((resolve, reject) => {
    let norm = ffmpeg("./download/" + fileName, { preset: "./preset" })
      .audioFilters([
        {
          filter: "loudnorm",
          options: "I=-" + intLoudness + ":TP=-3:LRA=" + lra,
        },
      ])
      .on("error", function (err) {
        console.error("Error occurred while processing file: " + err.message);
        const normResponse: normalizerResponse = {
          status: false,
          error: err,
        };
        return reject(normResponse);
      });

    if (configParams.bitrate != "") {
      norm.audioCodec("libmp3lame").audioBitrate(configParams.bitrate);
    } else {
      if (
        bitrate > 256 &&
        (formatResponse.format == "mp3" || formatResponse.format == "aac")
      ) {
        norm.audioCodec("libmp3lame").audioBitrate(256);
      } else {
        if (bitrate < 32 && formatResponse.format == "mp3") {
          norm.audioCodec("libmp3lame").audioBitrate(32);
        }
      }
    }

    norm
      .save("./processed/" + fileName + "." + formatResponse.format)
      .on("end", function (stdout, stderr) {
        console.log("File has been processed successfully");
        const normResponse: normalizerResponse = {
          status: true,
          fullFileName: fileName + "." + formatResponse.format,
        };
        return resolve(normResponse);
      });
    return norm;
  });
};

const bitrateCheck = async (fileName: string): Promise<number> => {
  return new Promise((resolve, reject) => {
    return ffmpeg.ffprobe("./download/" + fileName, (err, metadata) => {
      if (err) {
        return reject(err);
      } else {
        console.log("Bitrate: " + metadata["format"]["bit_rate"] / 1000);
        return resolve(metadata["format"]["bit_rate"] / 1000);
      }
    });
  });
};

const formatGetter = async (fileName: string): Promise<formatResponse> => {
  return new Promise((resolve, reject) => {
    return ffmpeg.ffprobe("./download/" + fileName, (err, metadata) => {
      let formatResponse: formatResponse;
      if (err) {
        return reject({
          error: err,
        });
      } else {
        console.log("Probed format: " + metadata["format"]["format_name"]);
        let fileFormat = metadata["format"]["format_name"];
        return resolve({
          format: fileFormat,
        });
      }
    });
  });
};
