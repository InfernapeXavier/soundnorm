import redis from "redis";
import { cacheResponse, configParams } from "../types";
import { promisify } from "util";

export const redisConnect = () => {
  const client = redis.createClient({
    retry_strategy: (options) => {
      if (options.error && options.error.code == "ECONNREFUSED") {
        return new Error(
          "Redis server refused connection to client, please try again."
        );
      }
    },
  });

  client.on("connect", function () {
    console.info("Connected to Redis");
  });

  client.on("error", function (err) {
    if (err.code == "CONNECTION_BROKEN") {
      console.log(
        "Redis server refused connection to client, please try again."
      );
      console.log("Redis " + err);
    }
  });

  return client;
};

export const cacheCheck = async (
  params: configParams,
  client: redis.RedisClient
): Promise<cacheResponse> => {
  const redisKey = JSON.stringify(params);
  const aGet = promisify(client.get).bind(client);

  const cacheGetResponse: string = await aGet(redisKey);

  if (cacheGetResponse != null) {
    console.log("Cached");
    // console.log("Client Response: " + cacheGetResponse);
    return {
      inCache: true,
      downloadUrl: cacheGetResponse,
    };
  } else {
    console.log("Not Cached");
    // console.log("Client Response: " + cacheGetResponse);
    return {
      inCache: false,
      downloadUrl: null,
    };
  }
};

export const cacheAdd = async (
  params: configParams,
  downloadUrl: string,
  client: redis.RedisClient
) => {
  const redisKey = JSON.stringify(params);
  const aSet = promisify(client.set).bind(client);

  console.log(downloadUrl);

  const cacheSetResponse: string = await aSet(redisKey, downloadUrl);
  return cacheSetResponse;
};
