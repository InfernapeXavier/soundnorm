import fs from "fs";
import backblaze from "backblaze-b2";
import { uploadResponse } from "../types";

export const fileUploader = async (fileName: string) => {
  const b2 = new backblaze({
    applicationKeyId: "000e9ac44b4e2950000000002",
    applicationKey: "K000qqPkXS26cefGp0FzDnubtIlHfic",
  });

  const authResponse = await b2.authorize();
  // const baseUrl = authResponse.data.downloadUrl; -> URL throws SSL Error
  const baseUrl: string = "https://soundnorm.s3.us-west-000.backblazeb2.com/";
//   console.log("Auth:" + authResponse.status);

  let response = await b2.getUploadUrl({
    bucketId: "6ea95a4c04d46b647e820915",
  });

  let uploadUrl = response.data.uploadUrl;
  let authToken = response.data.authorizationToken;
//   console.log("B2 URL:" + uploadUrl);

  let fileBuffer = fs.readFileSync("./processed/" + fileName);

  let uploadApiResponse = await b2.uploadFile({
    uploadUrl: uploadUrl,
    uploadAuthToken: authToken,
    fileName: fileName,
    data: fileBuffer,
  });

  const hostedUrl: string = baseUrl + uploadApiResponse.data.fileName;
  const uploaderResponse: uploadResponse = {
    apiResponse: uploadApiResponse,
    hostedUrl: hostedUrl,
  };

//   console.log("Upload: " + uploadApiResponse.status);
//   console.log("File Name: " + uploadApiResponse.data.fileName);
//   console.log("Download URL: " + hostedUrl);

  return uploaderResponse;
};
