import axios from "axios";
import fs from "fs";
import { configParams } from "../types";

// TODO Fingerprint File
export const fileDownloader = async (
  fileName: string,
  configParams: configParams
) => {
  const downloadFile = async (url: string) => {
    try {
      const response = await axios.get(url, {
        responseType: "stream",
      });
    //   console.log("DownloadGet: " + response.statusText);
      return response;
    } catch (err) {
      console.error(err);
    }
  };

  createDirs(); // Create the Directories required if they don't exist
  const downloadResponse = await downloadFile(configParams.urlInput);

  if (downloadResponse.status == 200) {
    const streamToFile = () => {
      return new Promise((resolve, reject) => {
        const fileWriteStream = fs.createWriteStream("./download/" + fileName);
        return downloadResponse.data
          .pipe(fileWriteStream)
          .on("error", (err: Error) => {
            return reject(err);
          })
          .on("finish", () => {
            return resolve(true);
          });
      });
    };

    let writeStreamResponse = await streamToFile().catch((err) => {
      console.error(err);
    });

    // console.log("WriteStream: " + writeStreamResponse);
    return true;
  } else {
    console.error(
      "Downloading file failed with response: " +
        downloadResponse.status +
        " = " +
        downloadResponse.statusText
    );
    return false;
  }
};

const createDirs = () => {
  let downloadDir = "./download";
  if (!fs.existsSync(downloadDir)) {
    fs.mkdirSync(downloadDir);
  }

  let processedDir = "./processed";
  if (!fs.existsSync(processedDir)) {
    fs.mkdirSync(processedDir);
  }
};
