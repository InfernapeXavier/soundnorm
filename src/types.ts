import redis from "redis";

export type configParams = {
  urlInput: string;
  intLoudness: string;
  lra: string;
  bitrate?: string;
};

export type cacheResponse = {
  inCache: boolean;
  downloadUrl?: string;
};

export type serverResponse = {
  inCache: boolean;
  downloadUrl: string;
};

export type uploadResponse = {
  apiResponse: any;
  hostedUrl: string;
};

export type normalizerResponse = {
  status: boolean;
  fullFileName?: string;
  error?: Error;
};

export type formatResponse = {
  format?: any;
  error?: Error;
};

export type redisConnectionResponse = {
  redisClient?: redis.RedisClient;
  status: boolean;
  error?: Error;
};
