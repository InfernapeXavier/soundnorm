import express from "express";
import { rootHandler, submitHandler } from "./handlers";

const app = express();
app.use(express.urlencoded({extended: true}));
app.use(express.json());
const port = process.env.PORT || "8000";

app.get("/", rootHandler);
app.post("/submit", submitHandler);

app.listen(port, () => {
  return console.log(`Server is listening on ${port}`);
});